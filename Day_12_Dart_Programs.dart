//abstract..interface..static
//collection ---list,set,map,queue,hashmap,hashset,linkedhashmap,linkedhashset,callable class

//abstract class..can't able to create object
//abstract method can only exist with abstract class
//need to override within sub class
//abstract class ---abstract method,normal method, instance variable
void main() {
  var di = NextClass();
  di.demo1();
}

abstract class DartIntermediate {
  void demo() {
    print("object");
  }

  void demo1();
}

class NextClass extends DartIntermediate {
  @override
  void demo1() {
    super.demo();
    print("override parent method");
  }
}
