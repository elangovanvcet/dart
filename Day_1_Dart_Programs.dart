import 'dart:io';

void main() {
  //datatypes
  //int,double --Numbers
  //String
  //Booleans
  //dynamic
  int a2 = 7;
  print(a2);
  double a = double.parse("8.8");
  double a1 = (9).toDouble();
  print(a);
  print(a1);
  double b = 8.8;
  print(b);
  String name = "Narmatha";
  print(name);
  bool font = true;
  print(font);

  //In dart everything is object
  //dart now null-safety...?
  bool? v;
  print(v);
  print("-------------dynamic---------------------");
  dynamic d = "Narmu";
  d = 66;
  print(d);
  dynamic d1 = 4;
  dynamic d2 = 4.6;
  dynamic d3 = true;
  print("-------------var---------------------");
  var v1 = "Narmatha";
  v1 = "Elango";
  print(v1);
  var v2 = 46;
  var v3 = 7.6;
  var v4 = false;

  String myName = 'Mango \'s is okay';
  print(myName);
  String myName1 = "Mango's is okay";
  print(myName1);
  String myName2 = r'Mango \"s is okay';
  print(myName2);
}
