//abstract..interface..static
//collection ---list,set,map,queue,hashmap,hashset,linkedhashmap,linkedhashset,callable class

//static--shared...only once memory created...in static methods only there relation only allowed
void main() {
  print(StaticConcept.a); //4 bytes
  StaticConcept.demo();
}

class StaticConcept {
  static int a = 7;
  int b = 3;
  static demo() {
    print(a);
    print("static method");
  }

  demo1() {
    print(a);
    print(b);
    demo();
    print("normal method");
  }
}
