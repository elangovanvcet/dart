import 'Exception.dart';

void main() {
  //exception --->its interupts the normal flow of an application
  //exception handling..try..catch...on..finally
  // try {
  //   int j = 10 ~/ 0; //infinity
  //   print(j);
  // }
// IntegerDivisionByZeroException
  // on UnsupportedError {
  //   print("pls dont divide by zero");
  // } catch (e) {
  //   print(e.toString());
  // } finally {
  //   print("always run");
  // }
  //custom exception handling
  try {
    test(91);
  } catch (e) {
    print(e);
  }
}
