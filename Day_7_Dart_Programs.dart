//lamda function or anonyms func..=>short hand notation or fat-arrow
//higher order function
//closure function
void main() {
  //lamda function..expression
  // Function c = (a, b) {
  //   print(a + b);
  // };
  // //=>short hand notation or fat-arrow
  // //Function c = (a, b) => print(a + b);
  // c(1, 2);

  //higher order function
  //ex: passing function to higher-order function
  // Function c = (a, b) => print(a + b);
  // String n = "aravind===>(love_today)_baskar";
  // newFunc(n, c);
  //receiving function from higher-order function
  // Function f = newFunc1(); //f-->h
  // print(f(10));
//closure -->within a closure we can modify the values of variables present in the parent scope
  String name = "O baskar"; //global
  click() {
    name = "pradeep"; //instance..local
    print(name);
  }

  print(name);
  click();
}

// //ex: passing function to higher-order function
// newFunc(String name, Function getFunc) {
//   print(name);
//   getFunc(2, 2); //c(2,2)
// }
//receiving function from higher-order function
// Function newFunc1() {
//   Function h = (a) => a + 6;
//   return h;
// }
