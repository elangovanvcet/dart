//abstract..interface..static
//collection ---list,set,map,queue,hashmap,hashset,linkedhashmap,linkedhashset,callable class

//there is no concept of interface...normal class acts as a interface
//we have to implements all the methods in class..not able to do with super keyword

void main() {
  var di = NextClass();
  di.demo1();
}

class DartIntermediate {
  void demo1() {}
  void demo2() {}
}

class DartIntermediate1 {
  void demo3() {
    print("parent");
  }

  void demo4() {
    print("parent1");
  }
}

class NextClass implements DartIntermediate, DartIntermediate1 {
  @override
  void demo1() {
    print("override grand parent method");
  }

  @override
  void demo2() {
    print("override grand parent method");
  }

  @override
  void demo3() {
    print("override parent method");
  }

  @override
  void demo4() {
    print("override parent method");
  }
}
