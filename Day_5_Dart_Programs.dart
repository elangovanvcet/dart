void main() {
  //labled statement

  outerForLoop:
  for (int i = 0, j = 0; i < 2; i++, j++) {
    print("i is $i and j is $j");
    for (int k = 0, l = 0; k < 2; k++, l++) {
      if (k == 1) {
        continue outerForLoop;
      }
      print("K is $k and L is $l");
    }
    print("i is $i and j is $j");
  }
}
