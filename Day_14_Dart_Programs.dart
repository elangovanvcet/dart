//mixins-->is a class whose methods and properties can be used by other classes without subclassing.
// it is reusable chunk of code that can be "plugged in" any classes...
void main() {
  var c1 = MixC();
  c1.divya(); //b class
  c1.narmu(); //c class
  c1.elango(); //a class
  c1.elango1(); //a class...if we use interface concepts we have too must override all those methods but with mixin there is compulsion
}

class MixA {
  elango() {
    print("elango");
  }

  elango1() {
    print("elangovan");
  }
}

class MixB {
  divya() {
    print("divya");
  }
}

class MixC extends MixB with MixA {
  narmu() {
    print("narmu");
  }
}
