//classe and objects
void main() {
  // var student1 = Student(); //student1 is a reference variable
  // var student1 = Student("patricia"); //parameter cons
  // student1.student_Id = 1;
  // student1.student_Name = "Narmu";
  // student1.student_Details();
  // student1.student1_Details();
  var student1 = Student("Sting");
  Student.myNamedConstructor("Micheal");
  Student.myNamedConstructor1("Maya");

  var student2 = Student("Undertaker");
  Student.myNamedConstructor("Shan");
  Student.myNamedConstructor1("prerna");
}

class Student {
  // int? student_Id; //instance_variable
  // String? student_Name;
  // student_Details() {
  //   //reference_variable.variable
  //   print("${this.student_Id} and ${this.student_Name} ");
  // }

  // int? student1_Id;
  // String? student1_Name;
  // student1_Details() {
  //   print("${student1_Id} and ${student1_Name} ");
  // }

  //default constructor..when the object is create the default cons is invoked
  // Student() {
  //   print("Hi this is default constructor");
  // }

//there is one rule ..its shuld be either def or para cons only can exist at one time not both

  //paramater constructor
  Student(String name) {
    print("Hi I'm $name");
  }
//Named Constructor
  Student.myNamedConstructor(String name) {
    print("Hi Patricia this is $name");
  }

  Student.myNamedConstructor1(String name) {
    print("Hi Patricia this is $name");
  }
}
