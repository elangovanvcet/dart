//collections --->list,set,map,queue,hashmap,hashset,linkedhashmap,linkedhashset,callable class

import 'dart:collection';

void main() {
  //2 types of list --->fixed list -  List l = [1, 2, 3, 4];,growable list...//t<int>--generic list
  // List l = List<int>.from([1, 2, 3, 4, "narmu", 9.00]);
  // List l1 = <int>[1, 2, 3, 4];
  // List l2 = [1, 2, 3, 4, 56, 7, 7];
  // // l.add(5);
  // // l.remove(1);
  // print(l2);
  // l2.forEach((e) {
  //   print(e);
  // });

  // l1.forEach((e) {
  //   print(e);
  // });

  // Set s = Set.from([1, 2, 3, 4]);
  // Set s1 = {1, 2, 3, 4};
  // s.add(5);
  // s.addAll([1, 2, 3, 4]);
  // s1.forEach((e) {
  //   print(e);
  // });
  // Set s2 = HashSet();
  // Set s3 = LinkedHashSet();

  // Map m = <int, dynamic>{
  //   1: "Narmu",
  //   2: 6,
  // };
  // //key =value
  // m[2] = "divya";
  // m.forEach((key, value) {
  //   print("key is $key and the value is $value");
  // });
  // Map m2 = HashMap();
  // m2.addAll({
  //   1: 3,
  // });
  // Map m3 = LinkedHashMap();

//   var queue = Queue<int>(); // ListQueue() by default
//   print(queue.runtimeType); // ListQueue

// // Adding items to queue
//   queue.addAll([1, 2, 3]);
//   queue.addFirst(0);
//   queue.addLast(10);
//   print(queue); // {0, 1, 2, 3, 10}

// // Removing items from queue
//   queue.removeFirst();
//   queue.removeLast();

//callable class
  var m = Man();
  m();
}

class Man {
  call() {
    print("callable class");
  }
}
