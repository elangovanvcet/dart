class StudentMark implements Exception {
  String threshold() {
    return "fail";
  }
}

test(int i) {
  if (i < 50) {
    throw new StudentMark().threshold();
  } else {
    print("you passed");
  }
}
