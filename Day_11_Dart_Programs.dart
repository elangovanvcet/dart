class AInheritance {
  //polymorphism -- two types-->function overloading and overriding
  //but in dart there is no concept of function overloading only func overriding is exist
  //parent
  int a = 8;

  AInheritance(String name1, String name2) {
    print("$name1");
    print("$name2");
  }
  AInheritance.myNameCon(String n, String n1) {
    print(n);
    print(n1);
  }
}

class BInheritance extends AInheritance {
  //sub class
  int b = 5;
  BInheritance(String name, String name1, String name2) : super(name1, name2) {
    print("joker$name");
  }
  BInheritance.myNameConstructor(String n, String n1) : super.myNameCon(n, n1) {
    print("guess aravind future wife name");
  }
}
