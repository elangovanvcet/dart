// void main() {
// //functions...in dart return-type is optional
//   int c = userName(3, 3); //functions as expression
//   print(c);
// }

// userName(int a, int b) {
//   return a + b;
// }
import 'dart:io';

void main() {
  //functions..4 types
  //1.required parameters
  //2.optional parameters
  //2.1=>positional optional parameter=[]
  //2.2=>named optional parameter={}
  //2.3=>default optional parameter={}
  //=============================
  // //1.required parameters
  // userName("hi");
  // //2.named parameters
  // userName(lastName: "hi", firstName: "bye");
  //3.default parameters
  // userName(lastName: "bye");
  // add(1, 2);
}

// userName(
//     {String? firstName = "please enter your firstname",
//     String? lastName = "please enter your lastname"}) {
//   print(firstName);
//   print(lastName);
// }

add(int a, int b) => print(a + b);
