//setter and getter,private variable..inheritance
class Ek {
  //private --- putting underscore infrontof
  String? _name = "Elango";

//Setters must declare exactly one required positional parameter.
  set setByName(String s) {
    print(_name);
    this._name = s;
    print(_name);
  }

  //getter
  get getByName {
    return _name;
  }
}
