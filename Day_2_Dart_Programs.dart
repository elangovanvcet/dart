import 'dart:io';

void main() {
  //String interpolation--dollar symbol
  String name = "Narmatha";
  print("what is my name $name");
  //const vs final
  //const and var can't be together
  const int i = 7; //must initialize the value
  // i = 6; //we can't able to change in compile time itself...
  print(i);
  final int
      j; //may be initialize the value or else we can initilaize in run-time also
  j = 9;
  print(j);
  //input and output --->'import dart:io'...!---> end of line
  stdout.write("what is my name ");
  int k = int.parse(stdin.readLineSync()!);
  print(k);
  //null operator
  int? l;
  print(l);
  //operators-->+,-,*,/,>>,<<,!=,&&,&,||,|,a>b ? true:false
  int a = 3; //0011
  int b = a >> 5; //right-shift--0001....(/2)
  print(b);
  int c = 3; //0011
  int d = a << 1; //left-shift--0110(*2)
  print(d);
  //type test
  var x = 7;
  if (x is int) {
    print("Integer");
  }
}
