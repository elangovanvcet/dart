class Amount {
  String? msg;
  int? n;
}

main() {
  //null-aware operator ---> .? , ?? , ??=
  var a;
  int? n1;
  String? message;
  if (a != null) {
    n1 = a.n ??= 0;
    message = a.msg ?? "wait";
  }
  print('$message and $n1');
}
