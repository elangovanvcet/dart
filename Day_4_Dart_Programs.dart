import 'dart:io';

void main() {
  // print("Loops...conditions..for loop");
  //conditions--->if..switch.
  // int a = 4;
  // int b = 2;
  // int c = 1;
  // if (a > b) {
  //   print("Hi");
  // } else if (b > c) {
  //   print("bye");
  // } else {
  //   print("ok");
  // }
  //==========================================
  // stdout.write("enter value : ");
  // String name = stdin.readLineSync()!;
  // String name1 = 'narmu'; //literals
  // switch (name) {
  //   case "elango":
  //     print("hai");
  //     break;
  //   case "1":
  //     print("bye");
  //     break;
  // }
  //==========================================
  // while (1 == true) {
  //   print(1);
  // }
  // while (1 == 1) {
  //   print(1);
  // }
//==========================================
  // int i = 0;
  // do {
  //   print("divya");
  //   i++;
  // } while (i <= 5);
  //==========================================
  //no concept of array
  var numbers = [1, 2, 3, 4, 5];
  // for (int i = 0; i < 5; i++) {
  //   print(i);
  // }
  //==========================================
  // for (var n in numbers) {
  //   print(n);
  // }
  //==========================================
  //lambda functions--new line
  numbers.forEach((divya) => print(divya));
  //lambda functions--same line
  numbers.forEach((divya) => stdout.write(divya));
  print("");
  //==========================================
  numbers.forEach((e) {
    //anonyms functions
    print(e);
  });
}
